

# Serempre: Test Devops React

This project was generated using [Nx](https://nx.dev).

## Run in local server all different environments:

Before running an environment, install project dependencies.

`$ npm install

Environment     | Command
--------------- | -------------
development     | npm run serve:dev
staging         | npm run serve:stag
production      | npm run serve:prod

## Build

Run `nx build my-app` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `nx test my-app` to execute the unit tests via [Jest](https://jestjs.io).

Run `nx affected:test` to execute the unit tests affected by a change.

## Running end-to-end tests

Run `nx e2e my-app` to execute the end-to-end tests via [Cypress](https://www.cypress.io).

Run `nx affected:e2e` to execute the end-to-end tests affected by a change.